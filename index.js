//EX 1
var sum = 0;
function inKQ(){
    for(var n = 1; sum < 10000; n++ ){
        sum += n;
        document.getElementById("res1").innerHTML = `Số nguyên n nhỏ nhất thỏa điều kiện là: n = ${n}`;
    }
}

//EX 2
function tinhTong(){
    var soX = document.getElementById("numX").value*1;
    var soN = document.getElementById("numN").value*1;
    var kq1 = 1;
    var kq2 = 0;
    for(var i = 0; i < soN; i++){
        kq1 *= soX;
        kq2 += kq1;
    }
    document.getElementById("res2").innerHTML = `Tổng là: ${kq2}`;
}

//EX 3
function tinhGiaiThua(){
    var numberN = document.getElementById("soN").value*1;
    var result = 1;
    for(i = 1; i <= numberN; i++){
        result *= i;
    }
    document.getElementById("res3").innerHTML = `Kết quả là: ${result}`;
}

//EX 4
function inDiv(){
var ABC = "";
var sumABC = "";
    for(var i = 1; i <= 10; i++){
        if(i % 2 == 0){
            ABC = `<div style= "background: red;color: white" class="w-50">Div chẵn ${i}</div>`;
        } else{
            ABC = `<div style= "background: blue;color: white" class="w-50">Div lẻ ${i}</div>`;
        }
        sumABC += ABC;
    }
    document.getElementById("res4").innerHTML = sumABC;
}